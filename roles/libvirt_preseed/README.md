Role Name
=========

Role for management of Virtual Machines with libvirt.

Requirements
------------

Debian with libvirt and hardware virtualization for Qemu/KVM.

Role Variables
--------------

Describe your machines:

```
machines:
    testvm:
        ram: 1024
        cpus: 1
        storage: 10
        net_config:
            token: 2
```

Configure the network environment.

```
net_config:
    wan_if: eth0
    bridge: virbr0
    domain: example.com
```


Dependencies
------------


- community.libvirt.virt

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

License
-------

BSD

Author Information
------------------

Alexander Böhm (alexander.boehm@malbolge.net)
https://github.com/aboehm
