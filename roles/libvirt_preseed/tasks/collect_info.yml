- name: get list of all vms
  virt:
    command: list_vms
  register: all_vms
  check_mode: no

- name: initialize facts
  set_fact:
    # collection of generated passwords for new VMs
    generated_passwords: {}
    # a list of VMs that will be generated
    machines_to_setup: []
    # period till a VMs have to be installed
    time_to_wait: 60

- name: Append machines and calculate time to wait
  set_fact:
    machines_to_setup: "{{ machines_to_setup + [item.key] }}"
    time_to_wait: "{{ time_to_wait |int + 60 }}"
  when: all_vms and not item.key in all_vms.list_vms |d([])
  loop: "{{ machines |dict2items }}"

- name: set defaults
  set_fact:
    net_config: "{{ default_net_config |combine(net_config |d({})) }}"
    debian_config: "{{ default_debian_config |combine(debian_config |d({})) }}"

- name: Check network config
  block:
    - name: "Ensure net_config.net is a valid address"
      fail:
        msg: "net_config.net='{{ net_config.net }}' isn't a valid IPv4 net"
      when: not net_config.net |ipv4

    - name: "Ensure net_config.net6 is a valid address"
      fail:
        msg: "net_config.net6='{{ net_config.net6 }}' isn't a valid IPv6 net"
      when: not net_config.net6 |ipv6

    - name: "Ensure net_config.gateway is a valid address"
      fail:
        msg: "net_config.gateway='{{ net_config.gateway }}' isn't a valid IPv4 address"
      when: not net_config.gateway |ipv4

    - name: "Ensure net_config.gateway6 is a valid address"
      fail:
        msg: "net_config.gateway6='{{ net_config.gateway6 }}' isn't a valid IPv6 address"
      when: not net_config.gateway6 |ipv6

    - name: "Ensure net_config.nameserver is a valid address"
      fail:
        msg: "net_config.nameserver='{{ net_config.nameserver }}' isn't a valid IPv4 address"
      when: not net_config.nameserver |ipv4

    - name: "Ensure net_config.nameserver6 is a valid address"
      fail:
        msg: "net_config.nameserver6='{{ net_config.nameserver6 }}' isn't a valid IPv6 address"
      when: not net_config.nameserver6 |ipv6

- name: Generate configuration for VMs
  set_fact:
    machines: |
      {{ machines |combine({
          item.key: default_machine_config |combine({
            'name': item.key,
            'uuid': 9999999999999999999999 |random |to_uuid,
            'extra_user': (default_extra_user |combine(item.value.extra_user, recursive=True)) if 'extra_user' in item.value
              else (default_extra_user |combine(extra_user |d({}), recursive=True)),
            'root_user': (default_root_user |combine(item.value.root_user, recursive=True)) if 'root_user' in item.value
              else (default_root_user |combine(root_user |d({}), recursive=True)),
            'debian_config': (default_debian_config |combine(item.value.debian_config, recursive=True)) if 'debian_config' in item.value
              else (default_debian_config |combine(debian_config |d({}), recursive=True)),
            'net_config': default_net_config | combine({
                'address': item.value.net_config.address |mandatory('IPv4 address must be set for machine'),
                'address6': item.value.net_config.address6 |mandatory('IPv6 address must be set for machine'),
                'publish_ports': {
                  'tcp': item.value.net_config.publish_ports.tcp |d([]),
                  'udp': item.value.net_config.publish_ports.udp |d([]),
                },
                'gateway': net_config.net |ipv4(1) |ipv4('address'),
                'nameserver': net_config.net |ipv4(1) |ipv4('address'),
                'gateway6': net_config.net6 |ipv6(1) |ipv6('address'),
                'nameserver6': net_config.net6 |ipv6(1) |ipv6('address'),
            }, recursive=True) |combine(item.value.net_config |d({}), recursive=true)
          }, recursive=True) |combine(item.value, recursive=true)
        }, recursive=True)
      }}
  loop: "{{ machines |dict2items }}"
