univention_templates
=========

This role uploads templates to a univention cororate server.
Templates are subsequently rendered to files and
the configuration is activated.

Requirements
------------

none

Role Variables
--------------

There is a single variable defined:

* `ucr_templates` - Defines the files to be uploaded.

Dependencies
------------

none


Example Playbook
----------------

    - hosts: ucs.domain.tld
      roles:
         - univention_templates

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
