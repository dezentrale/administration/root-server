# Dezentrale Server

## Dokumentation

Weiterführende Dokumentation findet sich im Unterverzeichnis `./doc`:

[Dokumentation](doc/documentation.md)

## Dienste

### Feststehende Dienste

|**Anwendung**                   | **Realisierung** |
|---                             |---               |
|Authentifizierung & Selfservice |UCS               |
|Dokumentenablage                |Nextcloud via UCS |
|Kalender                        |Nextcloud via UCS |
|Kanban                          |Wekan via UCS     |
|Schlüsselverwaltung             |Bitwarden via UCS |
|Mail                            |UCS               |
|Member-Mail                     |UCS               |

### Offene Diskussionen zu Diensten

* Mailing-Listen
    * Realisierung über UCS für Member möglich
    * für Public-Listen (Self-Subscribe) muss noch eine Lösung gefunden werden
* Wiki
    [ ] altes Mediawiki
    [ ] Markdown über Nextcloud/Git?
    [x] Wiki VM angelegt
* Matrix-Client -> Nextcloud App, oder etwas anderes?
* Git
    * Gitlab -> hat schön CI Unterstützung, ist aber sehr fordernd
    [x] gitea installiert
* Konferenz
    * Jitsi?
    * BBB?
* Projekt-Space (für eigene Projekte der Member)
* Web Space (Dezentrale Seite, Statische Inhalte)


## Grundlegende Einrichtung

Installation von Ansible inklusive Abhängigkeiten

```
# Debian/Ubuntu
apt install python3 
pip3 install --user ansible netaddr
ansible-galaxy install -r requirements.yml
```

Ausführen von Ansible

```
ansible-playbook <PLAYBOOK>
```

Um via SSH und Ansible die Hosts zu verwalten sollte noch in der *ssh config* folgende Hosts konfiguriert werden und *<USER>* durch den verwendenten User ersetzt werden:

```
Host hw4f.dezentrale.space
    Port 4022

Host *.dezentrale.cloud
    User <USER>
    ProxyJump hw4f.dezentrale.space
```

## Playbooks

- **deploy-bootstrap.yml** Deployment user aufsetzen und mit allen hinterlegten deployment Keys konfigurieren 
- **user-reset-password.yml** Generiert ein zufälliges Passwort für einen reglären User 
- **user-setup.yml** Reguläre user einrichten und ggf. sudo einrichten
- **proxy.yml** Host mappings für Proxy (TCP/UDP + HTTP/HTTPS routing) erstellen
- **setup-vms.yml** VMs erstellen und grundlegend provisionieren

### Playbook deploy-bootstrap.yml

Es wird *sudo* und *python3* installiert, ein User *deploy* angelegt, ihm *sudo*-Rechte gegeben und SSH-Keys hinterlegt.
Die Keys werden über die User Datenbank *vars/users.yml* aufgelöst, wenn die Option *deploy=yes* gesetzt ist.

### Playbook user-reset-passord.yml

Wird genutzt von *scripts/reset-password*. Als notwendiges Variable muss *user* angegeben werden.
 
### Playbook user-setup.yml

Es wird *sudo* und *python3* installiert und alle User eingerichtet, die in *vars/users.yml* aufgelistet sind.
Ist die Option *sudo=yes* gesetzt, so wird dem User sudo rechte gegeben.

#### Aufbau der User Datenbank

Pro Dictonary Eintrag gilt:

```yaml
<USERNAME>:
    name: <DISPLAY NAME>    # (Optional)
    ssh_keys:
        - <KEY 1>           # (Notwendig)
        - <KEY 2>           # (Optional)
        - ...
    sudo: <yes|no>          # (Optional, Standard no)
    deploy: <yes|no>        # (Optional, Standard no)
```


### Playbook setup-vms.yml

Erzeugt VMs nach der Defintions in hosts.yml aus der Grupp *virtual_machines*. Es wird anhand des Feldes *token* eine IP-Adresse gesetzt (192.168.122.<token>, fd00::<token>) und ein Port-Forwarding auf dem Host eingerichtet für die Einträge *tcp* und *udp* in *publish_ports*.

#### Definition VMs

VMs werden als Gruppe in Inventory *hosts.yml* konfiguriert. Ist ein Eintrag vorhanden, wird eine VM mit dem Name des Eintrags erstellt und die VM-Spezifika aus dem Eintrag zur Erstellung übernommen. Weitere Felder werden von anderen Playbooks zur Konfiguration von Diensten herangezogen, bspw. für die Konfiguration des HTTP-Proxy.

Im folgenden Beispiel wird eine VM *vm.example.com* mit 1GB RAM, 10GB Festplatte und 2 CPUs erstellt. Die VM erhält die IP-Addressen *192.168.122.42* und *fd00::42*. Vom Host werden der TCP-Port 8080 auf den Port 80 und der UDP-Port 1149 auf die VM weitergeleietet. 

Es wird der HTTP-Proxy für die Domains *vm.example.com*, *app.my-domain.com*, siehe Playbook *proxy.yml*.

```yaml
vm.example.com:
  ram: 1024
  cpus: 2
  storage: 20
  net_config:
    token: 42
    publish_ports:
      tcp:
        - "8080:80"
      udp:
        - 1149
    publish_web:
      aliases:
        - app.my-domain.com
        - foo.bar
```

### Playbook proxy.yml

Konfiguriert einen transparenten HTTP und TLS-SNI-Proxy mit haproxy. Die Weiterleitung findet anhand konfigurierten IP-Addresse des *token* erstellt. Es wird der Name des Hosts weitergeleitet, wenn das Feld *publish_web* existiert. Wenn darin ein weitere Liste *aliases* vorhanden ist, wird die darin enthaltenen Domainnamen ebenfalls weitergeleitet.

Für die Weiterleitung muss keinerlei Zertifikat angepasst werden. Die TLS-Verschlüsselung muss auf der Ziel-VM eingerichtet sein.

## Scripts

* *scripts/deluser* Entfernt einer User von einem/r ansible manged Host(gruppe)
* *scripts/reset-password* Erzeugt ein neues Passwort für einer User auf einem/r ansible manged Host(gruppe)

Die Scripts benötigen Zugriff auf den *deploy* User, d.h. in der User-Datenbank muss *deploy: yes* gesetzt und angewendet sein.

## Spezifische Konfiguration für UCS Dienste

### Onlyoffice in Nextcloud integrieren

Generelles Vorgehen: 
OnlyOffice unter UCS installieren.
"ONLYOFFICE"-App in Nextcloud installieren.
Liszensdatei unter /var/lib/univention-appcenter/apps/onlyoffice-ds-integration/Data/license.lic ablegen.
ggf. user, gruppe und rechte der Liszensdatei anpassen.

Problem: Onlyoffice kann nicht in Nextcloud aktiviert werden.


Unter UCS -> DNS zwei Zonen anlegen:

* Für UCS selbst
    * Name: ucs.dezentrale.cloud
    * Nameserver: ucs.dezentrale.cloud
    * IP-Adresse: 192.168.122.3
* Für Nextcloud
    * Name: next.dezentrale.cloud
    * Nameserver: next.dezentrale.cloud
    * IP-Adresse: 192.168.122.3


Danach Container für Nextcloud und Onlyoffice neustarten

```
univention-app restart nextcloud
univention-app restart onlyoffice-ds-integration
```

Sollte Probleme gegeben, kann man über ```univenation-app shell <APP>``` in den Container wechsel und den Aufruf mittels *curl* überprüfen, bspw:

```
# univention-app shell onlyoffice-ds-integration
root@onlyo-60011935:/# curl https://next.dezentrale.cloud
<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html><head>
<title>302 Found</title>
</head><body>
<h1>Found</h1>
<p>The document has moved <a href="https://next.dezentrale.cloud/nextcloud">here</a>.</p>
<hr>
<address>Apache/2.4.25 (Univention) Server at next.dezentrale.cloud Port 443</address>
</body></html>
```

## Single Sign ON

### Anpassungen bei UCR Variablen

|**Variable**                                | **Wert**                                                                  |
|---                                         |---                                                                        |
|saml/idp/entityID                           |https://ucs-sso.dezentrale.cloud/univention/saml/metadata                  |
|saml/idp/enableSAML20-IdP                   |true                                                                       |
|saml/idp/https                              |true                                                                       |
|saml/idp/ldap/get_attributes                |'uid', 'mailPrimaryAddress', 'memberOf', 'enabledServiceProviderIdentifier'|
|saml/idp/ldap/search_attributes             |'uid', 'mailPrimaryAddress'                                                |
|umc/saml/idp-server                         |https://ucs-sso.dezentrale.cloud/simplesamlphp/saml2/idp/metadata.php      |
|umc/saml/sp-server                          |(leer)                                                                     |
|umc/saml/trusted/sp/ucs.dezentrale.intranet |ucs.dezentrale.intranet                                                    |

Variablen fürs Debuggen:

|**Variable**               |**Wert** |
|---                        |---      |
|saml/idp/ldap/debug        |true     |
|saml/idp/log/debug/enabled |true     |
|saml/idp/log/level         |DEBUG    |

### Apache-Konfiguration

Das Playbook *univention_templates.yml* hinterlegt die nötwendigen Anpassungen.

### Nextcloud einbinden

**Anpassungen in UCS**

Über Plugin SAML SAML Service Provider muss ein neuer Dienst hinzugefügt werden, die über UCS User authentifizieren kann.

|**Feld**                                                               |**Wert**                                                          |
|---                                                                    |---                                                               |
|Service Provider aktivieren:                                           |X                                                                 |
Bezeichner des Service Providers (Name mit der sich Nextcloud ausweist) |https://next.dezentrale.cloud/nextcloud/apps/user_saml/saml/login |
Antwort an diese Service Provider URL nach dem Login                    |https://next.dezentrale.cloud/nextcloud/apps/user_saml/saml/acs   |
Single logout URL des Service Providers                                 |https://next.dezentrale.cloud/nextcloud/apps/user_saml/saml/sls   |
Format des NameID Attributs                                             |urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified             |
Name des Attributs, das als NameID verwendet wird                       |uid                                                               |
Name der Organisation des Service Providers                             |Dezentrale                                                        |
Enable signed Logouts                                                   |X                                                                 |

Das Zertifikat für UCS als Provider muss heruntergeladen werden und später in die Nextcloud eingefügt werdne. Es ist über den Link an der Seite https://ucs.dezentrale.cloud/simplesamlphp/saml2/idp/certificate erreichbar.

Weiter muss für die User der Provider freigeschaltet werden über Gruppen -> SAML Einstellungen -> Gruppe für folgende Service Provider freischalten -> Eintrag ```https://next.dezentrale.cloud/nextcloud/apps/user_saml/saml/metadata``` hinzufügen.

**Anpassungen in Nextcloud**

Über dasPlugin SSO & SAML-Autorisierung in den Admintrationseinstellungen muss ein Authorisier hinzugefügt werden

|**Feld**                                                                                                     |**Wert**                                                                        |
|---                                                                                                          |---                                                                             |
|Anmeldung nur erlauben, wenn ein Konto auf einem anderen Backend vorhanden ist (z.B. LDAP)                   |X                                                                               |
|SAML-Autorisierung für die dezentrale Desktop-Clients nutzen (erfordert eine Neu-Autorisierung der Benutzer) |X                                                                               |
|Die Verwendung von mehreren Benutzerverwaltungen erlauben (z. B. LDAP)                                       |X                                                                               |
|Attribut dem die UID zugeordnet werden soll                                                                  |uid                                                                             |
|Optional den Namen des Identitätsanbieters anzeigen (Standard: "SSO- & SAML-Anmeldung")                      |UCS                                                                             |
|Identifikationsmerkmal des Autorisierungsdienstes (muss URI sein)                                            |https://ucs-sso.dezentrale.cloud/univention/saml/metadata                       |
|URL-Ziel des Autorisierungsdienstes an den der Dienstanbieter die Anmeldungsanfrage senden soll              |https://ucs-sso.dezentrale.cloud/simplesamlphp/saml2/idp/SSOService.php         |
|Optional Identity Provider Settings                                                                          |                                                                                |
|URL-Adresse des Autorisierungsdienstes an den der Diensteanbieter die SLO-Anfrage senden soll:               |https://ucs-sso.dezentrale.cloud/simplesamlphp/saml2/idp/SingleLogoutService.php|
|Öffentliches X.509-Zertifikat des Autorisierungsdienstes                                                     |Einfügen des heruntergeladenen Zertifikats                                      |

Es muss *Metadaten gültig* erscheinen bzw. über den Button *Lade Metadaten-XML herunter* kann die XML mit den Mapping heruntergeladen werden, welches ```urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified``` sein muss.

