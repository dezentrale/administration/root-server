## Nextcloud

[Zurück zur Dokumentaion](documentation.md)

### Shell Zugriff

Die Nextcloud ist ein Docker Conatainer auf dem Univention Corporate Server.
Shell Zugriff enthält man über Univention Kommandos
```bash
univention-app shell nextcloud
```
oder mit Docker Boartmitteln
```bash
CONT_NC="$(docker container ls \
	|grep nextcloud
	|sed -r 's/.+\ ([^ ]+)$/\1/')"
docker container exec -it "$CONT_NC" /bin/bash
```

Die Shell einfach mit `exit` oder `^d` verlassen.
Mit `sudo` können erhöhte Rechte angefordert werden.

### Cli management

Auf dem CLI wird die NextCloud mit dem Kommando `occ` administriert.
Dieses befindet sich im Container unter `/var/www/html/occ` und
muss vom Nutzer ausgeführt werden,
welcher die Datei `./config/config.php` besitzt,
in diesem Fall `www-data`.

Mit folgendem Befehl findet sich ein einfacher Einstieg in das Nextcloud-Management.
```bash
sudo -u www-data \
	/var/www/html/occ --help
```

### Logging

Die Nextcloud kann beispielsweise
nach einem automatischen Upgrade Fehler werfen.
Zunächst schreibt sie ein Update-Log nach
`/var/log/nextcloud-upgrade_*.log`


Unter erhöhten Rechten kann
das laufende Logbuch kann eingesehen werden unter.
```bash
su - www-data -s /bin/bash
cd /var/www/html
./occ log:tail -f |tee /tmp/nc.log
```

Alternativ kann auch `sudo` verwendet werden
```bash
sudo -u www-data \
	/var/www/html/occ log:tail -f \
	|tee /tmp/nc.log
```

Damit können Fehler identifiziert werden.


### App-Management

`occ` bietet eine Reihe von Befehlen zum Management von Nextcloud Apps an.
```bash
 app
  app:check-code                         check code to be compliant
  app:disable                            disable an app
  app:enable                             enable an app
  app:getpath                            Get an absolute path to the app directory
  app:install                            install an app
  app:list                               List all available apps
  app:remove                             remove an app
  app:update                             update an app or all apps
```

#### Deaktiverung von Apps

Wenn die Nextcloud einen Fehler wirft,
der durch ein Plugin verursacht wird,
bietet es sich an dieses Plugin zu deaktivieren
um die Verfügbarkeit der NextCloud wieder zu gewährleisten.

```bash
su - www-data
cd /var/www/html
./occ app:disable spreed
```


#### Manuelles Upgrade einer App

Apps sind letztlich nur Verzeichnisse unterhalb von `/var/www/html/apps/`.
Und können auch direkt installiert und aktualisiert werden.

Dieses Vorgehen war notwendig um einen Fehler zu beseitigen,
der ein Management der Nextcloud über das Webfrontend unmöglich machte.
[After NC Upgrade from 20.0.7 to 20.0.8 "Internal Server Error" and SQL Error "'oc_talk_participants' doesn't exist" · Issue #5324 · nextcloud/spreed
 - [GitHub](https://github.com/nextcloud/spreed/issues/5324)

Beispiel manuelles Upgrade der Nextcloud-App `spreed` (Talk)
```bash
su www-data
cd /tmp
wget -O https://github.com/nextcloud/spreed/releases/download/v10.1.3/spreed-10.1.3.tar.gz
tar -xzf spreed-10.1.3.tar.gz
mv /var/www/html/spreed/ /tmp/spreed_old; mv /tmp/spreed /var/www/html/apps/
```
